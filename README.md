# steamcache-primer

Windows Batch script using SteamCMD to download/prime a SteamCache with a list of games.

Place the Batch file and applist.txt in the same folder as your SteamCMD.exe, populate the applist.txt with a list of games (by depot/appid) one per line and then run the batch script.

** Must be placed in the same directory as SteamCMD.exe! **